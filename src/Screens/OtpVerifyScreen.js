import React, {useState, useRef} from 'react';
import {Text, TextInput, TouchableOpacity, View, SafeAreaView, StyleSheet } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { FirebaseRecaptchaVerifierModal } from 'expo-firebase-recaptcha';
import firebase from './firebase';

export default App = () => {

  const [phoneNumber, setPhoneNumber] = useState('');
  const [code, setCode] = useState('');
  const [verificationId, setVerificationId] = useState(null);

  const recaptchaVerifier = useRef(null);

  const sendVerification = () => {
    const phoneProvider = new firebase.auth.PhoneAuthProvider();
    phoneProvider
      .verifyPhoneNumber(phoneNumber, recaptchaVerifier.current)
      .then(setVerificationId);
  };
  const confirmCode = () => {
    const credential = firebase.auth.PhoneAuthProvider.credential(
      verificationId,
      code
    );
    firebase
      .auth()
      .signInWithCredential(credential)
      .then((result) => {
        // Do something with the results here
        console.log(result);
      });
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={{ alignSelf: 'flex-start', height: '15%', width: '100%', marginLeft: '5%'}}>
        <Ionicons name="arrow-back-circle-sharp" size={40} color="orange" />
      </View>
      <View style={{ marginLeft: '20%', width: '80%' }}>
        <Text style={{ fontSize: 25, fontWeight: 'bold'}}>OTP Verification Code</Text>
        <Text style={{ marginTop: '2%', fontSize: 18}}>An automated 6 digit verification</Text>
        <Text style={{ fontSize: 18}}>code will be received shortly to your mobile number through sms</Text>
      </View>
      <TextInput
        style={{ fontSize: 20, marginBottom: '1%', marginTop: '5%', borderBottomColor: 'grey', borderBottomWidth: 1, width: '65%'}}
        placeholder="Phone Number"
        onChangeText={setPhoneNumber}
        keyboardType="phone-pad"
        autoCompleteType="tel"
      />

      <View style={{ alignSelf: 'center', marginTop: '3%', backgroundColor: 'blue', width: '70%', height: '7%', borderRadius: 25, alignItems: 'center', justifyContent: 'center'}}>
        <TouchableOpacity
        onPress={sendVerification}
        style={{ width: '100%', height: '100%', alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }} >
          <Text style={{ fontSize: 20, color: 'white'}}>Send Verification</Text>
        </TouchableOpacity>
      </View>


      {/* <TouchableOpacity
      style={{ width: '100%', alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}
      onPress={sendVerification}>
        <Text>Send Verification</Text>
      </TouchableOpacity> */}

      {/* Verification Code Input */}
      <TextInput
        style={{ fontSize: 20, marginBottom: '1%', marginTop: '10%', borderBottomColor: 'grey', borderBottomWidth: 1, width: '65%'}}
        placeholder="Confirmation Code"
        onChangeText={setCode}
        keyboardType="number-pad"
      />

      <View style={{ alignSelf: 'center', marginTop: '3%', backgroundColor: 'green', width: '70%', height: '7%', borderRadius: 25, alignItems: 'center', justifyContent: 'center'}}>
        <TouchableOpacity
        onPress={confirmCode}
        style={{ width: '100%', height: '100%', alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }} >
          <Text style={{ fontSize: 20, color: 'white'}}>Verify Code</Text>
        </TouchableOpacity>
      </View>

      {/* <TouchableOpacity onPress={confirmCode}>
        <Text>Send Verification</Text>
      </TouchableOpacity> */}

      <FirebaseRecaptchaVerifierModal
        ref={recaptchaVerifier}
        firebaseConfig={firebase.app().options}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    // justifyContent: 'center',
    marginTop: '10%',
    width: '100%',
    height: '100%',
  },

  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
  },
 
  underlineStyleHighLighted: {
    borderColor: "#03DAC6",
  },
});
